package ca.aquiletour.server;

import java.nio.file.Path;
import java.nio.file.Paths;

import ca.ntro.core.system.log.Log;
import ca.ntro.core.system.trace.T;
import ca.ntro.jdk.services.EarlyInitializationJdk;
import ca.ntro.services.ConfigService;

public class EarlyInitializationServer extends EarlyInitializationJdk {
	
	private String configPath = null;
	
	public EarlyInitializationServer(String[] args) {
		if(args.length > 0) {
			this.configPath = args[0];
		}
	}

	@Override
	protected ConfigService provideConfigService() {
		T.call(this);
		
		Path configFilepath = null;
		
		if(configPath != null) {
			
			configFilepath = Paths.get(configPath);
			
		}else {

			String userHome = System.getProperty("user.home");
			configFilepath = Paths.get(userHome, ".aiguilleur", "config.json");

		}

		System.out.println("[CONFIG] Loading config file: " + configFilepath.toAbsolutePath().toString());

		AquiletourConfig config = AquiletourConfig.loadFromJson(configFilepath);
		
		return config;
	}

}
