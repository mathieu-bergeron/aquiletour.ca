package ca.aquiletour.server.vertx;


import ca.aquiletour.server.EarlyInitializationServer;
import ca.aquiletour.server.LocalStoreServerDev;
import ca.aquiletour.server.LocalStoreServerProd;
import ca.aquiletour.server.MessageServiceWebserver;
import ca.aquiletour.server.backend.AquiletourBackendService;
import ca.aquiletour.web.AquiletourRouterService;
import ca.ntro.jdk.web.NtroWebServer;
import ca.ntro.services.Ntro;

public class JavaMainVertx {
	
	public static void main(String[] args) {

		NtroWebServer.defaultInitializationTask(new EarlyInitializationServer(args),
												AquiletourBackendService.class, 
				                                LocalStoreServerDev.class,
				                                LocalStoreServerProd.class,
				                                MessageServiceWebserver.class, 
				                                new AquiletourRouterService())
		             .setOptions(args)
		             .addNextTask(new AquiletourMainServerVertx())
		             .execute();
		             //.execute().addGraphWriter(new GraphTraceWriterJdk(new File("TMP")));
	}
}
