# TODO

Config DNS importante pour ne pas être indentifié comme SPAM:

https://www.digitalocean.com/community/tutorials/how-to-install-and-configure-dkim-with-postfix-on-debian-wheezy

La Config DNS semble très importante: https://www.cloudflare.com/learning/dns/dns-records/dns-dkim-record/

## NOTES

1. Bon d'avoir un FQDN, incluant un hostname, comme mail.aiguilleur.ca

1. En plus de postfix, il faut opendkim et un DMARC

## POSTFIX

1. Ne pas exécuter en chroot (fichier master.cf)
    * pas besoin de copier des fichiers dans /var/spool/postfix

## Tester opendkim

https://dkimcore.org/c/keycheck


```
opendkim-genkey  -r -s 2023 -d aiguilleur.ca --directory=/root/opendkim
```




# Config actuelle mail.aiguilleur.ca


1. hostname mail.aiguilleur.ca

1. config nginx

    * mail.aiguilleur.ca:4443/mail  pour le client Web Roundcude Webmail (pas nécessaire en réalité)

        include /etc/nginx/templates/roundcube.tmpl;


    * Roundcube mail est dans /opt/www/roundcubemail/$1 (en PHP)

1. dovecot.service: IMAP/POP3 email server (pas nécessaire en réalité. on veut uniquement STMP)

1. config dovecot

    * c'est là qu'on utiliser le certificat SSL de mail.aiguilleur.ca

    * (celui fournit par lestencrypt)

    ```txt
     # SSL: Global settings.
     # Refer to wiki site for per protocol, ip, server name SSL settings:
     # http://wiki2.dovecot.org/SSL/DovecotConfiguration
     ssl_min_protocol = TLSv1.2
     ssl = required
     verbose_ssl = no
     ssl_dh = </etc/pki/tls/dh2048_param.pem
     ssl_cert = </etc/letsencrypt/live/mail.aiguilleur.ca/fullchain.pem
     ssl_key = </etc/letsencrypt/live/mail.aiguilleur.ca/privkey.pem

     # Fix 'The Logjam Attack'
     ssl_cipher_list = EECDH+AESGCM:EDH+AESGCM:AES256+EECDH:AES256+EDH
     ssl_prefer_server_ciphers = yes

     disable_plaintext_auth = yes
    ```

1. postfix.service: Postfix Mail Transport Agent (le seul nécessaire... à mettre dans un Dockers)
    

1. config postfix (via MySQL?)

    relay_domains: aiguilleur.ca (transport: dovecot? nécessaire?)

    ```txt
    smtpd_tls_key_file = /etc/letsencrypt/live/mail.aiguilleur.ca/privkey.pem
    smtpd_tls_cert_file = /etc/letsencrypt/live/mail.aiguilleur.ca/cert.pem
    smtpd_tls_CAfile = /etc/letsencrypt/live/mail.aiguilleur.ca/chain.pem
    smtpd_tls_CApath = /etc/letsencrypt/live/mail.aiguilleur.ca

    #
    # Disable SSLv2, SSLv3
    #
    smtpd_tls_protocols = !SSLv2 !SSLv3
    smtpd_tls_mandatory_protocols = !SSLv2 !SSLv3
    smtp_tls_protocols = !SSLv2 !SSLv3
    smtp_tls_mandatory_protocols = !SSLv2 !SSLv3
    lmtp_tls_protocols = !SSLv2 !SSLv3
    lmtp_tls_mandatory_protocols = !SSLv2 !SSLv3

    #
    # Fix 'The Logjam Attack'.
    #
    smtpd_tls_exclude_ciphers = aNULL, eNULL, EXPORT, DES, RC4, MD5, PSK, aECDH, EDH-DSS-DES-CBC3-SHA, EDH-RSA-DES-CDC3-SHA, KRB5-DE5, CBC3-SHA
    smtpd_tls_dh512_param_file = /etc/pki/tls/dh512_param.pem
    smtpd_tls_dh1024_param_file = /etc/pki/tls/dh2048_param.pem

    tls_random_source = dev:/dev/urandom
    ```


    

1. config DNS

* MX mail.aiguilleur.ca (priorité 10)
* TXT v=spf1 mx ~all  
* TXT 123423_domainkey     (???)

1. Dans le code, voici les infos dont on a besoin:

    ```java
    String fromEmail = config.getSmtpFrom();
    String username = config.getSmtpUser();
    String password = config.getSmtpPassword();

    Properties prop = new Properties();
               prop.put("mail.smtp.host", config.getSmtpHost());
               prop.put("mail.smtp.port", config.getSmtpPort());
               prop.put("mail.smtp.auth", "true");
               prop.put("mail.smtp.starttls.enable", String.valueOf(config.getSmtpTls()));
   ```

   ```json
    "smtpHost": "mail.aiguilleur.ca",
    "smtpPort": "587",
    "smtpTls": true,
    "smtpUser": "equipe@aiguilleur.ca",
    "smtpFrom": "equipe@aiguilleur.ca",
    "smtpPassword": "XXXX",
   ```







