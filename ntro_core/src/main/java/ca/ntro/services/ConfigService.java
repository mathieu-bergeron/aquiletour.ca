package ca.ntro.services;

public class ConfigService {

	public boolean isProd() {
		return false;
	}

	public boolean useHttps() {
		return false;
	}

	public boolean useSslWebSocket() {
		return false;
	}

	public boolean useMongoDb() {
		return false;
	}

	public boolean useProdLogging() {
		return false;
	}
}
