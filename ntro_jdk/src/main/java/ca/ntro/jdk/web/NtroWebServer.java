package ca.ntro.jdk.web;

import ca.ntro.core.Constants;
import ca.ntro.core.system.log.Log;
import ca.ntro.core.system.trace.__T;
import ca.ntro.jdk.services.BackendServiceServer;
import ca.ntro.services.EarlyInitialization;
import ca.ntro.services.MessageService;
import ca.ntro.services.ModelStore;
import ca.ntro.services.Ntro;
import ca.ntro.services.NtroInitializationTask;
import ca.ntro.services.RouterService;

public class NtroWebServer {

	public static NtroInitializationTask defaultInitializationTask(EarlyInitialization earlyInitialization,
																   Class<? extends BackendServiceServer> backendServiceClass, 
			                                                       Class<? extends ModelStore> modelStoreClassDev,
			                                                       Class<? extends ModelStore> modelStoreClassProd,
			                                                       Class<? extends MessageService> messageServiceClass,
			                                                       RouterService routerService) {
		__T.call(NtroWebServer.class, "defaultInitializationTask");

		earlyInitialization.performInitialization();
		
		System.out.println("[CONFIG] useHttps: " + Ntro.config().useHttps());
		System.out.println("[CONFIG] useSslWebSocket: " + Ntro.config().useSslWebSocket());
		System.out.println("[CONFIG] useMongoDb: " + Ntro.config().useMongoDb());
		System.out.println("[CONFIG] useProdLogging: " + Ntro.config().useProdLogging());

		
		NtroInitializationTask initializationTask = new NtroInitializationTask();
		initializationTask.setTaskId(Constants.INITIALIZATION_TASK_ID);

		initializationTask.addSubTask(new InitializationTaskWebserver(backendServiceClass, 
				                                                      modelStoreClassDev, 
				                                                      modelStoreClassProd, 
				                                                      messageServiceClass, 
				                                                      routerService));
		

		return initializationTask;
	}

}
